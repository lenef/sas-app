FROM cloudron/base:3.0.0@sha256:455c70428723e3a823198c57472785437eb6eab082e79b3ff04ea584faf46e92 

RUN mkdir -p /app/code 
WORKDIR /app/code

ARG SAS_VERSION=1.0.2

RUN apt-get update && apt-get install -y openjdk-11-jre-headless 
            
RUN rm -rf /var/cache/apt /var/lib/apt/lists

# Install  

RUN wget https://github.com/glenrobson/SimpleAnnotationServer/releases/download/${SAS_VERSION}/sas.zip -O /tmp/sas.zip  

RUN unzip /tmp/sas.zip -d /tmp 
RUN rm -Rf /app/code/*
RUN cp -Rp /tmp/sas/* /app/code/
RUN rm -Rf /tmp/sas* 

RUN ln -s /app/data data

RUN chown -R cloudron:cloudron /app/code 

# add code

COPY start.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]
