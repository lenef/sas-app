This app packages  SimpleAnnotationServer 

This is an Annotation Server which is compatible with IIIF and Mirador. This Annotation Server includes a copy of Mirador so you can get started creating annotations straight away. The annotations are stored as linked data in an Apache Jena triple store by default.
