#!/bin/bash

set -eu -o pipefail

mkdir -p /app/data/

LIMIT=$(($(cat /sys/fs/cgroup/memory/memory.memsw.limit_in_bytes)/2**20))
export JAVA_OPTS="-XX:MaxRAM=${LIMIT}M"

chown -R cloudron:cloudron /app/data

echo "==> Starting SimpleAnnotationServer"

exec gosu cloudron:cloudron java ${JAVA_OPTS} -jar /app/code/dependency/jetty-runner.jar --port 8182 /app/code/simpleAnnotationStore.war
